var Grid = require('../../noqq-msngr/grid'),
    grid = new Grid('logger', 'util');
   

grid.on('unknown', function(data){
    console.log('unknown', data);
});
grid.on('log', function(data){
    console.log(data);
});
grid.on('err', function(data){
    if(data.e){
        console.log(data.e); 
        return;
    } 
    console.log(data);
});
grid.on('nodeAdded', function(node){
    console.log('\nnodes:');
    console.log(grid.nodes);
});

grid.on('nodeRemoved', function(node){
    console.log('\nnodes:');
    console.log(grid.nodes);
});
