var Grid = require('../../noqq-msngr/grid'),
    grid = new Grid('plugins', 'network');
    
grid.admin = Math.random();
console.log('admin: ', grid.admin);
    
var requireDir = require('require-dir'),
    _ = require('underscore');

var pluginsContainer = {},
    plugins = requireDir('../../plugins');

_.each(plugins, function(plugin, key, list){
    pluginsContainer[key]  = plugin.plugin;
});
console.log(pluginsContainer);
grid.on('message', function(data){
    if(data.message.indexOf('!') === 0){
        var args = data.message.split(' '),
            msgData = {
                plugin: args[0].substr(1),
                args: args,
                message: data.message
            };

        if(_.has(pluginsContainer, msgData.plugin)){
            pluginsContainer[msgData.plugin](grid, msgData.args, msgData.message);
        }
    }
});
grid.on('registerPlugin', function(data){
    try {
        var p = require('../../plugins/'+data.pluginToRegister);
        pluginsContainer[data.pluginToRegister] = p.plugin;
        console.log(pluginsContainer);
    } catch(e){
        console.log(e);
    }
});