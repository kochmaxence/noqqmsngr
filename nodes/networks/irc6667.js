var Grid = require('../../noqq-msngr/grid'),
    grid = new Grid('irc6667', 'network');
   
var irc = require('irc'),
    _ = require('underscore');

var client = new irc.Client('irc.6667.eu', 'noqqz', {
    channels: ['#noqq']
});

var roster = [];

client.on('error', function(err){
    log('err', { e: err });
});

client.on('registered', function(raw){
    log('log', { status: 'online' });
});
client.on('names#noqq', function(nicks){
    _.each(nicks, function(nick, index, l){
        roster.push(nick);
    });
});
client.on('join#noqq', function(nick, raw){
    rosterAdd(nick, function(){
        grid.transmit({
            type: 'join',
            name: nick
        });
    });
});
client.on('nick', function(oldNick, newNick, channels){
    rosterUpd(oldNick, newNick, function(){
        grid.transmit({
            type: 'name',
            name: newNick,
            oldName: oldNick
        });
    });
});
client.on('message#noqq', function(nick, text, raw){
    grid.transmit({
        type: 'message',
        name: nick,
        message: text
    });
});
client.on('part', function(nick, reason, raw){
    rosterDel(nick, function(){
        grid.transmit({
            type: 'leave',
            name: nick
        });
    });
});

grid.on('join', function(data){
    broadcast('['+data.from+'] '+data.name+' joined.');
});
grid.on('name', function(data){
    broadcast('['+data.from+'] '+data.oldName+' is now '+data.name+'.');
});
grid.on('message', function(data){
    broadcast('['+data.from+'] '+data.name+': '+data.message);
});
grid.on('leave', function(data){
    broadcast('['+data.from+'] '+data.name+' left.');
});
grid.on('plugin', function(data){
    broadcast('['+data.plugin+'] ' + data.message);
});
grid.on('requestRoster', function(data){
    grid.transmit({
        to: data.from,
        type: 'roster',
        roster: roster
    });
});

function broadcast(message){
    client.say('#noqq', message);
}

function log(type, data){
    grid.transmit({
        type: type,
        to: 'logger',
        message: data
    });
}

function rosterAdd(name, cb){
    if(!_.contains(roster, name)){
        roster.push(name);
        if(typeof cb === 'function'){
            cb(name, roster);
        }
    }
}
function rosterDel(name, cb){
    if(_.contains(roster, name)){
        roster.splice(roster.indexOf(name), 1);
        if(typeof cb === 'function'){
            cb(name, roster);
        }
    }
}
function rosterUpd(oldName, newName, cb){
    if(_.contains(roster, oldName)){
        roster[roster.indexOf(oldName)] = newName;
        if(typeof cb === 'function'){
            cb(newName, roster);
        }
    }  
}