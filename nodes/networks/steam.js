var Grid = require('../../noqq-msngr/grid'),
    grid = new Grid('steam', 'network');
   
var steam = require('steam'),
    _ = require('underscore');

var client = new steam.SteamClient(),
    chatroomId = '103582791434034994';
    
client.logOn('##', '##');
client.on('loggedOn', function(){
    client.setPersonaState(steam.EPersonaState.Online);
    client.setPersonaName('noqqz');
    client.joinChat(chatroomId);
    log('log', { status: 'online' });
});

var roster = [];

client.on('error', function(err){
    log('err', { e: err });
});

client.on('chatStateChange', function(type, targetId, roomId, shooterId){
    switch(type){
        case steam.EChatMemberStateChange.Entered:
            rosterAdd(client.users[targetId].playerName, function(){
                grid.transmit({
                    type: 'join',
                    name: client.users[targetId].playerName
                });
            });
        break;
        case steam.EChatMemberStateChange.Disconnected:
            rosterDel(client.users[targetId].playerName, function(){
                grid.transmit({
                    type: 'leave',
                    name: client.users[targetId].playerName
                });
            });
        break;
        case steam.EChatMemberStateChange.Left:
            rosterDel(client.users[targetId].playerName, function(){
                grid.transmit({
                    type: 'leave',
                    name: client.users[targetId].playerName
                });
            });
        break;
        case steam.EChatMemberStateChange.Kicked:
            rosterDel(client.users[targetId].playerName, function(){
                grid.transmit({
                    type: 'message',
                    name: 'ADMIN',
                    message: client.users[targetId].playerName + ' has been kicked by ' + client.users[shooterId].playerName
                });
            });
        break;
        case steam.EChatMemberStateChange.Banned:
            rosterDel(client.users[targetId].playerName, function(){
                grid.transmit({
                    type: 'message',
                    name: 'ADMIN',
                    message: client.users[targetId].playerName + ' has been banned by ' + client.users[shooterId].playerName
                });
            });
        break;
    }
});

client.on('chatMsg', function(roomId, message, type, chatterId){
    grid.transmit({
        type: 'message',
        name: client.users[chatterId].playerName,
        message: message
    });
});

grid.on('join', function(data){
    broadcast('['+data.from+'] '+data.name+' joined.');
});
grid.on('name', function(data){
    broadcast('['+data.from+'] '+data.oldName+' is now '+data.name+'.');
});
grid.on('message', function(data){
    broadcast('['+data.from+'] '+data.name+': '+data.message);
});
grid.on('leave', function(data){
    broadcast('['+data.from+'] '+data.name+' left.');
});
grid.on('plugin', function(data){
    broadcast('['+data.plugin+'] ' + data.message);
});
grid.on('requestRoster', function(data){
    grid.transmit({
        to: data.from,
        type: 'roster',
        roster: roster
    });
});
function broadcast(message){
    client.sendMessage('103582791434034994', message);
}

function rosterAdd(name, cb){
    if(!_.contains(roster, name)){
        roster.push(name);
        if(typeof cb === 'function'){
            cb(name, roster);
        }
    }
}
function rosterDel(name, cb){
    if(_.contains(roster, name)){
        roster.splice(roster.indexOf(name), 1);
        if(typeof cb === 'function'){
            cb(name, roster);
        }
    }
}
function rosterUpd(oldName, newName, cb){
    if(_.contains(roster, oldName)){
        roster[roster.indexOf(oldName)] = newName;
        if(typeof cb === 'function'){
            cb(newName, roster);
        }
    }  
}

function log(type, data){
    grid.transmit({
        type: type,
        to: 'logger',
        message: data
    });
}