var Grid = require('../../noqq-msngr/grid'),
    grid = new Grid('lol', 'network');
   
var xmpp = require('../../lib/node-xmpp'),
    _ = require('underscore');

var client = new xmpp.Client({
        jid: '##',
        password: '##',
        host: 'chat.eu.lol.riotgames.com',
        port: 5223,
        legacySSL: true
    }),
    roomJid = 'pu~c34f1c96122653d34cc424abc066c3b7030d6408@lvl.pvp.net',
    roomNick = 'noqqz';

var roster = [];

client.on('error', function(err){
    log('err', { e: err });
});

client.on('online', function(){
    client.send(new xmpp.Element('presence', {}).
        c('show').t('chat').up().
        c('status').t('<body><profileIcon>28</profileIcon><statusMsg>NoQQ Messenger</statusMsg><level>1</level><wins>1337</wins><leaves>0</leaves><queueType>RANKED_SOLO_5x5</queueType><rankedWins>1337</rankedWins><rankedLosses>0</rankedLosses><rankedRating>1337</rankedRating><tier>PLATINUM</tier><skinname>Annie</skinname><gameStatus>inGame</gameStatus><timeStamp>1317686957096</timeStamp></body>')
    );
    client.send(new xmpp.Element('presence', { to: roomJid +'/'+ roomNick }).
        c('x', { xmlns: 'http://jabber.org/protocol/muc' })
    );
    log('log', { status: 'online' });
    setInterval(function() {
        client.send(new xmpp.Element('presence'));
    }, 1000 * 10);
});
client.on('stanza', function(stanza){
    parseStanza(stanza, function(data){
        switch(data.type){
            case 'error':
                log({ type: 'error', err: stanza });
            break;
            case 'presence':
                if(data.status == 'available'){
                    rosterAdd(data.name, function(){
                        grid.transmit({
                            type: 'join',
                            name: data.name
                        });
                    });
                } else if(data.status == 'unavailable'){
                    rosterDel(data.name, function(){
                        grid.transmit({
                            type: 'leave',
                            name: data.name
                        });
                    });
                }
            break;
            case 'chat':
                
            break;
            case 'groupchat':
                grid.transmit({
                    type: 'message',
                    name: data.name,
                    message: data.message
                });
            break;
            default:
                return;
        }
    });
});

grid.on('join', function(data){
    broadcast('['+data.from+'] '+data.name+' joined.');
});
grid.on('name', function(data){
    broadcast('['+data.from+'] '+data.oldName+' is now '+data.name+'.');
});
grid.on('message', function(data){
    broadcast('['+data.from+'] '+data.name+': '+data.message);
});
grid.on('leave', function(data){
    broadcast('['+data.from+'] '+data.name+' left.');
});
grid.on('plugin', function(data){
    broadcast('['+data.plugin+'] ' + data.message);
});
grid.on('requestRoster', function(data){
    grid.transmit({
        to: data.from,
        type: 'roster',
        roster: roster
    });
});

function broadcast(message){
    client.send(new xmpp.Element('message', { to: roomJid, type: 'groupchat'}).
        c('body').t(message)
    );
}

function log(type, data){
    grid.transmit({
        type: type,
        to: 'logger',
        message: data
    });
}

function rosterAdd(name, cb){
    if(!_.contains(roster, name)){
        roster.push(name);
        if(typeof cb === 'function'){
            cb(name, roster);
        }
    }
}
function rosterDel(name, cb){
    if(_.contains(roster, name)){
        roster.splice(roster.indexOf(name), 1);
        if(typeof cb === 'function'){
            cb(name, roster);
        }
    }
}
function rosterUpd(oldName, newName, cb){
    if(_.contains(roster, oldName)){
        roster[roster.indexOf(oldName)] = newName;
        if(typeof cb === 'function'){
            cb(newName, roster);
        }
    }  
}

function parseStanza(stanza, cb){
    var data = {},
        text;
    if(stanza.attrs.type == 'error'){
        data.type = 'error';
    } else if(stanza.is('presence')){
        data.type = 'presence';
        if(stanza.attrs.from.split('/')[0] == roomJid && stanza.attrs.from !== roomJid+'/'+roomNick){
            data.name = stanza.attrs.from.split('/')[1];
            if(stanza.attrs.type == 'unavailable'){
                data.status = 'unavailable';
            } else if(stanza.getChild('show') && stanza.getChild('show').getText() == 'chat'){
                data.status = 'available';
            }
        }
    } else if(stanza.is('message')){
        if(stanza.attrs.from !== roomJid+'/'+roomNick){
            if(stanza.attrs.type == 'chat'){
                text = stanza.getChild('body').getText();
                data.type = 'chat';
                data.message = text;
            } else if(stanza.attrs.type == 'groupchat'){
                text = stanza.getChild('body').getText();
                data.type = 'groupchat';
                data.name = stanza.attrs.from.split('/')[1];
                if(text.indexOf('<![[CDATA')){
                    data.message = text.slice(9, text.length - 3);
                } else {
                    data.message = text;
                }
            }   
        }
    }
    
    cb(data);
}