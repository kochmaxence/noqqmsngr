var _ = require('underscore');

exports.plugin = function(grid, args, message){
    console.log(grid.nodes);
    var msg = '';
    _.each(grid.nodes['network'], function(oNetwork, sKey, oList){
        if(sKey !== 'plugins'){
            msg += '{' + sKey + '} ';
        }
    });
    grid.transmit({
        type: 'plugin',
        to: 'network',
        plugin: 'networks',
        message: msg
    });
};