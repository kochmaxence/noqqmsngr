exports.plugin = function(grid, args, message){
    grid.transmit({
        type: 'plugin',
        to: 'network',
        plugin: 'help',
        message: '!help, !infos, !version, !networks, !roster [networkName]'
    });
};