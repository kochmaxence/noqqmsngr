
exports.plugin = function(grid, args, message){
    grid.transmit({
        type: 'plugin',
        to: 'network',
        plugin: 'version',
        message: 'NoQQ Messenger Version 4'
    });
};