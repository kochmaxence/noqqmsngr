var _ = require('underscore');

exports.plugin = function(grid, args, message){
    if(args[1] && _.has(grid.nodes['network'], args[1])){
        var msg = '';
        grid.transmit({
            type: 'requestRoster',
            to: args[1],
        });
        grid.once('roster', function(data){
            if(_.size(data.roster) > 0){
                _.each(data.roster, function(name, index, l){
                    msg += '['+name+'] ';
                });
                grid.transmit({
                    type: 'plugin',
                    to: 'network',
                    plugin: 'roster',
                    message: msg
                });    
            } else {
                grid.transmit({
                    type: 'plugin',
                    to: 'network',
                    plugin: 'roster',
                    message: 'There\'s no one down there!'
                });
            }
            grid.transmit({
                type: 'plugin',
                to: 'network',
                plugin: 'roster',
                message: 'Some networks need users to (re-)join the room to be in the list.'
            });
        });    
    }
};