exports.plugin = function(grid, args, message){
    if(args[2] == grid.admin){
        if(args[1]){
            grid.transmit({
                type: 'registerPlugin',
                to: 'plugins',
                plugin: 'registerplugin',
                pluginToRegister: args[1]
            });
        }
        grid.admin = Math.random();
        console.log('admin: ', grid.admin);
    }
};