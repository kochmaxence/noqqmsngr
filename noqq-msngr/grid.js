var Discover = require('node-discover'),
    EventEmitter = require('events').EventEmitter,
    utils = require('util');

var iDisco = module.exports = function(instName, instType){
    this.nodes = {};
    this.nodes[instType] = {};
    this.nodes[instType][instName] = { name: instName, type: instType };
    EventEmitter.call(this);
    var that = this;
    this.instName = instName;
    this.instType = instType;
    this.d = new Discover();
    this.d.advertise({
        name: instName,
        type: instType
    });
    this.d.on('added', function(node){
        that.nodes[node.advertisement.type][node.advertisement.name] = {
            name: node.advertisement.name, 
            type: node.advertisement.type 
        };
        that.emit('nodeAdded', node);
    });
    this.d.on('removed', function(node){
        delete that.nodes[node.advertisement.type][node.advertisement.name];
        that.emit('nodeRemoved', node);
    });
    this.d.on('promotion', function(node){
        that.emit('promoted', node);
    });
    this.d.on('demotion', function(node){
        that.emit('demotion', node);
    });
    this.d.on('master', function(node){
        that.emit('newMaster', node); 
    });
    
    this.d.join(instName, function(data){
        that.emit(data.type, data);
    });
    this.d.join(instType, function(data){
        that.emit(data.type, data); 
    });
    this.joinChannel = function(channel){
        that.d.join(channel, function(data){
            that.emit(data.type, data);
        });
    }
    
    this.transmit = function(data){
        data.type = data.type || 'unknown';
        data.to = data.to || instType;
        data.from = data.from || instName;
        that.d.send(data.to, data);
    };
    
};
utils.inherits(iDisco, EventEmitter);